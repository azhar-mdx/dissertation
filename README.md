**Robotics Dissertation Project**

This project is developed and submitted for a thesis in the field of Robotics for the Degree of Master of Science.

This repository contains all the codes, instructions and guidance to operate and configure the robot with user controller devices.

The project presents completely remote-controlled vehicle invention. The scope of the project is to operate a tank vehicle using a remote controller and mobile-based virtual reality headset having the capabilities of telepresence. This project is merely a prototype and does not have all components of telepresence. The vehicle is operated completely remotely, meaning the user would operate this vehicle via internet and view its surroundings using the virtual reality headset with the help of camera present on the vehicle. The vehicle and the user are completely independent in terms of their locations and the only thing required for both is high-speed internet. This project is based on 4G internet, however in future, with the use of 5G this project can be much more efficient. The technology used to communicate between the vehicle and the user is ROS bridge, which is a part of ROS. ROS bridge connects native and non-ROS compatible devices using web sockets, which enables non-ROS devices to communicate with native ROS devices. The vehicle possesses a Raspberry PI along with a camera module, 2 SG90 Servo motors, 2 DC motors, some necessary modules and sensors. The project utilizes sensors of the mobile device which is used in virtual reality headset to identify user head movements and orientation. The camera present on the vehicle rotates based on the collected data from the mobile device.

---

## To configure or operate the robot use one of the following links

1. To operate the robot, please read the instructions here: [User manual](https://bitbucket.org/azhar-mdx/dissertation/src/master/user-manual/).
2. To configure the robot on a new machine, please read the instructions here: [Installation process](https://bitbucket.org/azhar-mdx/dissertation/src/master/installation-process/).

*There is also a demo video available on YouTube. Follow this link to view the video: [Watch the demo video](https://youtu.be/hbNScSrsoiQ).*

---