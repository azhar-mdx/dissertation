**Instructions to setup and configure the robot (Installation process)**

This is a installation process manual. The user manual to operate the robot can be found [here](https://bitbucket.org/azhar-mdx/dissertation/src/master/user-manual/).

---

## Setting up the server

1. The very first thing is to setup the server. In this project the server was setup on a Raspberry Pi, it can also be hosted on any Ubuntu 16.04 machine or even online.
2. If setting up on Raspberry Pi then it's a good idea to use an image of Ubuntu, this saves time and comes with all necessary installations. The image that was used in this project is by Ubiquity Robotics which can be found [here](https://downloads.ubiquityrobotics.com/). This imsage comes with pre-configured ROS kinetic.
3. Once the server machine is ready, the next step is to install ROS Bridge on it.
4. Install the ROS Bridge using following command in terminal: "sudo apt-get install ros-<rosdistro>-rosbridge-server"
5. Once ROS Bridge is installed, source it using following command: "source /opt/ros/<rosdistro>/setup.bash"
6. If everything went well in previous steps, you should be able to run ROS Bridge using following command: "roslaunch rosbridge_server rosbridge_websocket.launch"

---

## Setting up Arduino

1. Install the arduino IDE on a PC. Use the following [link](https://www.arduino.cc/en/main/software) to download the software.
2. Connect Arduino Nano to the PC and then run Arduino IDE.
3. Select the correct board and port config in the IDE and then upload [this sketch](https://bitbucket.org/azhar-mdx/dissertation/src/master/arduino-codes/).
4. Once the upload is done, disconnect the Arduino from PC and connect it with the Robot's RPi.

---

## Setting up Robot's Raspberry Pi

1. Same as the server, install the ubuntu image by Ubiquity Robotics which can be found [here](https://downloads.ubiquityrobotics.com/). This imsage comes with pre-configured ROS kinetic.
2. To communicate with ROS Bridge, we require ros bridge library for python. To install this library use the following command: "pip install roslibpy"
3. To communicate with Arduino, we require serial library for python. To install this library use the following command: "pip install pyserial"
4. After installing these dependencies, you should be able to use [this script](https://bitbucket.org/azhar-mdx/dissertation/src/master/rpi-python-scripts/) to communicate with ROS Bridge and Arduino.
5. Next we set up RWS service for live video stream. To install the RWS, first download the .deb package from [here](https://github.com/kclyu/rpi-webrtc-streamer-deb).
6. Install the package using following command in terminal: "sudo dpkg -i rws_xxx_armhf.deb"
7. Once installed, you should be able to start the service using following command: "sudo systemctl start rws"
8. Check the status of the service using following command in terminal: "sudo systemctl status rws"
9. Replace the pre-installed "native-peerconnection" example with [these codes](https://bitbucket.org/azhar-mdx/dissertation/src/master/rws-p2p/).
10. If everything went well in previous steps, you should be able to communicate with server and view the live stream in a mobile app. See the next section to install and use the mobile app.

---

## Installing and using the mobile app

1. These are the steps required to use the mobile app on Android devices. First download the .apk file on an Android device from [here](https://bitbucket.org/azhar-mdx/dissertation/src/master/android-apk/). ALternatively, the .apk file can be download on a PC and then copied to the mobile device.
2. Open the .apk file on the mobile device and install the app. If a warning pops up that the source is not trusted, then go into settings and check the box which says something like "trust unknown apps".
3. Run the app from home screen.

---

## Making changes in mobile app (Windows)

1. The mobile app is built with Ionic Framework. In order to make changes to the mobile app, you will need to install Ionic Framework first. To do so run the following command in the terminal: "npm install -g cordova ionic"
2. Download the mobile app codes from [here](https://bitbucket.org/azhar-mdx/dissertation/src/master/mobile-app/)
3. Open up a terminal and change directory to the mobile app codes folder.
4. First install all the dependencies by running "npm i" in a terminal.
5. Next follow the instructions in "android-ionic-config.pdf".
6. Once android setup is done with ionic, then run the following command in terminal: "ionic cordova prepare android". This will add all the configs which were done previously to the project and setup android mobile app on your PC.
7. You can now make changes to the app file present inside the src folder.
8. You can now run the app on your mobile device by using following command: "ionic cordova run android". For live reload while working on code add -l flag to the command. For example "ionic cordova run android -l".
9. Alternatively, you can serve the app from your PC and use on the mobile phone via ionic DevApp. To do so, follow the instructions in [ionic docs](https://ionicframework.com/docs/appflow/devapp).

---