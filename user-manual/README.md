**Instructions to operate the robot**

This is a user manual to operate the robot. To configure and install dependencies, please visit this [link](https://bitbucket.org/azhar-mdx/dissertation/src/master/installation-process/).

---

## Running the server

1. Switch on the server Raspberry Pi by connecting it to any power source.
2. Connect an HDMI and check if it is connected to a network (be default it connects to MDX Robotics).
3. Note the IP address of the server.
4. Run the ROS Bridge server by using following command in terminal: "roslaunch rosbridge_server rosbridge_websocket.launch". The default port for ROS Bridge is 9090. However, 9090 was already in use and then port was changed to 9091.

---

## Running the scripts and services on robot's RPi to communicate with server

1. Connect the power bank to robot's Raspberry Pi.
2. Connect an HDMI to robot's Pi and check if it is connected to a network (be default it connects to MDX Robotics).
3. Note the IP address of the robot's Pi.
4. Check the RWS service status on robot's Pi by typing following command in terminal "sudo systemctl status rws", if the service is active then nothing else needs to be done. Otherwise if the service is disabled, then simply start it with following command "sudo systemctl start rws". The default port for RWS in 8889.
5. Open the script "ros-bridge-to-arduino.py" which is placed on desktop.
6. Scroll down and check if the server IP address written in the script matches the actual IP address of server. If it does not match then simply update the host IP address in the script.
7. Run the python script using following command in terminal "sudo python3.6 ros-bridge-to-arduino.py".
8. Switch on the power for motor driver module which is located in the centre of the robot.

---

## Using the mobile application

1. Open the mobile application on 2 different mobile devices (Since everything is on localhost, make sure you are connected to the same network as server and Robot's RPi).
2. Enter the IP addresses including ports for both server and Robot's RPi.
3. Connect to server and stream by tapping on button connect.
4. Assign a controller to each mobile device.
5. Put the head movement controller device in VR glasses and use navigation controller device to navigate the robot.
6. Enjoy!

---