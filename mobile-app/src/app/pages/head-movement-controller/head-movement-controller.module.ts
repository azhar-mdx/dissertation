import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HeadMovementControllerPage } from './head-movement-controller.page';

const routes: Routes = [
  {
    path: '',
    component: HeadMovementControllerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HeadMovementControllerPage]
})
export class HeadMovementControllerPageModule {}
