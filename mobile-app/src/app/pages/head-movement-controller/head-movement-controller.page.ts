import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DeviceMotionAccelerationData } from '@ionic-native/device-motion/ngx';
import { RosService } from '../../services/ros/ros.service';
import { HelperService } from '../../services/helper/helper.service';

@Component({
  selector: 'app-head-movement-controller',
  templateUrl: './head-movement-controller.page.html',
  styleUrls: ['./head-movement-controller.page.scss'],
})
export class HeadMovementControllerPage implements OnInit {

  panServoAngle: number = 0;
  tiltServoAngle: number = 0;
  streamIP: any;

  constructor(private MathHelper: HelperService, private ros: RosService, private ds: DomSanitizer) { }

  ngOnInit() {
    this.streamIP = this.ds.bypassSecurityTrustResourceUrl('http://' + this.ros.streamIP + ':' + this.ros.streamPort + '/native-peerconnection/');
    this.ros.createPublishersForServo();
    this.accelerationData();
    this.orientation();
  }

  // Get Pan Servo Data
  orientation() {
    window.addEventListener('deviceorientation', ((ev: any) => { // adding a lsitener to listen for value changes in device orientation
      let angle = this.MathHelper.headingCalculator(ev.alpha, ev.beta, ev.gamma); // taking values (alpha, beta and gamma) from device orientation and converting into compass heading
      if (angle !== this.panServoAngle) { // publish data if new value is not equal to previous value(s)
        if (angle >= 20 && angle <= 160) { // publish data if new value is greater than 20 and less than 160 degrees
          this.ros.servoPanPublisher(angle); // publish the value to ROS topic
        }
      }
      this.panServoAngle = angle; // save new value to a variable
    }));
  }

  // Get Tilt Servo Data
  accelerationData() {
    this.ros.accelerometer().subscribe((orientation: DeviceMotionAccelerationData) => { // adding a subscriber to listen for value changes in device acceleration
      let angle = this.MathHelper.mappingHelper(orientation.z, -10, 10, 0, 180); // taking z-axis value and converting into angle
      if (angle !== this.tiltServoAngle) { // publish data if new value is not equal to previous value(s)
        if (angle >= 20 && angle <= 160) { // publish data if new value is greater than 20 and less than 160 degrees
          this.ros.servoTiltPublisher(angle); // publish the value to ROS topic
        }
      }
      this.tiltServoAngle = angle;  // save new value to a variable
    });
  }

}
