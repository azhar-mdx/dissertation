import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadMovementControllerPage } from './head-movement-controller.page';

describe('HeadMovementControllerPage', () => {
  let component: HeadMovementControllerPage;
  let fixture: ComponentFixture<HeadMovementControllerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadMovementControllerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadMovementControllerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
