import { Component, OnInit } from '@angular/core';
import { RosService } from '../../services/ros/ros.service';
import { DeviceMotionAccelerationData } from '@ionic-native/device-motion/ngx';

@Component({
  selector: 'app-navigation-controller',
  templateUrl: './navigation-controller.page.html',
  styleUrls: ['./navigation-controller.page.scss'],
})
export class NavigationControllerPage implements OnInit {

  navigationValues: any;

  constructor(private ros: RosService) { }

  ngOnInit() {
    this.ros.createPublishersForNav();
    this.getNavigation();
  }

  getNavigation() {
    this.ros.accelerometer().subscribe((orientation: DeviceMotionAccelerationData) => { // adding a subscriber to listen for value changes in device acceleration
      let vel = { x: orientation.x, y: orientation.y, z: orientation.z }; // taking acceleration data
      this.ros.navPublisher(vel); // publishing the acceleration data to ROS Topic
      this.navigationValues = vel; // saving acceleration data to a variable which displays data on screen
    });
  }

}
