import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationControllerPage } from './navigation-controller.page';

describe('NavigationControllerPage', () => {
  let component: NavigationControllerPage;
  let fixture: ComponentFixture<NavigationControllerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationControllerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationControllerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
