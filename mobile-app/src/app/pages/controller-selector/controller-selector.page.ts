import { Component, OnInit } from '@angular/core';
import { RosService } from '../../services/ros/ros.service';

@Component({
  selector: 'app-controller-selector',
  templateUrl: './controller-selector.page.html',
  styleUrls: ['./controller-selector.page.scss'],
})
export class ControllerSelectorPage implements OnInit {

  constructor(private ros: RosService) { }

  ngOnInit() {
  }

  connect() {
    this.ros.connectRosServer();
  }

}
