import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ControllerSelectorPage } from './controller-selector.page';

const routes: Routes = [
  {
    path: '',
    component: ControllerSelectorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ControllerSelectorPage]
})
export class ControllerSelectorPageModule {}
