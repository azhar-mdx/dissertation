import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllerSelectorPage } from './controller-selector.page';

describe('ControllerSelectorPage', () => {
  let component: ControllerSelectorPage;
  let fixture: ComponentFixture<ControllerSelectorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllerSelectorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControllerSelectorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
