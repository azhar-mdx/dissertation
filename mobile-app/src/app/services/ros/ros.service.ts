import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { DeviceMotion, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion/ngx';

declare const EventEmitter2: any;
declare const ROSLIB: any;

@Injectable({
  providedIn: 'root'
})
export class RosService {

  rosObject: any;
  rosConnection: boolean;
  rosServerIP: string;
  rosServerPort: string;

  streamIP: string;
  streamPort: string;

  panPubObj: any;
  tiltPubObj: any;
  velPubObj: any;

  constructor(private deviceMotion: DeviceMotion, public alertController: AlertController) {
    this.rosServerIP = '192.168.';
    this.rosServerPort = '9091';
    this.streamIP = '192.168.5.91';
    this.streamPort = '8889';
  }

  // Error alert
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Connection failed',
      message: 'Unable to connect to ROS Bridge server. Please check your connection or make sure the IP address is correct and try again.',
      buttons: ['OK']
    });

    await alert.present();
  }

  // ROS Server Connection
  connectRosServer() {
    this.rosObject = new ROSLIB.Ros();
    // If there is an error on the backend, an 'error' emit will be emitted.
    this.rosObject.on('error', (error) => {
      console.log(error);
      this.presentAlert();
    });
    // When we made a connection.
    this.rosObject.on('connection', () => {
      console.log('Connection made!');
      this.rosConnection = true;
    });
    // When a connection is closed.
    this.rosObject.on('close', () => {
      console.log('Connection closed.');
      this.rosConnection = false;
    });
    // Create a connection to the rosbridge WebSocket server.
    this.rosObject.connect('ws://' + this.rosServerIP + ':' + this.rosServerPort);
  }

  createPublishersForServo() {
    this.panPubObj = this.publisherObject('/servo_pan', 'std_msgs/Int16');
    this.tiltPubObj = this.publisherObject('/servo_tilt', 'std_msgs/Int16');
  }

  createPublishersForNav() {
    this.velPubObj = this.publisherObject('/vr_robo_vel', 'geometry_msgs/Point');
  }

  // Create New ROS Publisher Object
  publisherObject(topicName: any, msgType: any) {
    return new ROSLIB.Topic({
      ros: this.rosObject,
      name: topicName,
      messageType: msgType
    });
  }

  // X-axis Head Movement Publisher Object
  servoPanPublisher(angle: any) {
    let data = new ROSLIB.Message({ data: angle });
    this.publishData(this.panPubObj, data);
  }

  // Y-axis Head Movement Publisher Object
  servoTiltPublisher(angle: any) {
    let data = new ROSLIB.Message({ data: angle });
    this.publishData(this.tiltPubObj, data);
  }

  // Navigation Publisher Object
  navPublisher(axis: any) {
    let data = new ROSLIB.Message(axis);
    this.publishData(this.velPubObj, data);
  }

  // ROS Data Publisher
  publishData(topic: any, data: any) {
    this.rosConnection ? topic.publish(data) : console.log('Unable to publish, ROS server is unavailable');
  }

  // Accelerometer sensor values
  accelerometer() {
    let options: DeviceMotionAccelerometerOptions = { frequency: 40 };
    return this.deviceMotion.watchAcceleration(options);
  }

}
