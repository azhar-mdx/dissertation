import { TestBed } from '@angular/core/testing';

import { RosService } from './ros.service';

describe('RosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RosService = TestBed.get(RosService);
    expect(service).toBeTruthy();
  });
});
