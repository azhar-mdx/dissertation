import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }

  headingCalculator(a, b, g) {
    // Convert Degrees to Radians
    let alphaRad = a * (Math.PI / 180);
    let betaRad = b * (Math.PI / 180);
    let gammaRad = g * (Math.PI / 180);
    // Calculate Equation Components
    let cA = Math.cos(alphaRad);
    let sA = Math.sin(alphaRad);
    let cB = Math.cos(betaRad);
    let sB = Math.sin(betaRad);
    let cG = Math.cos(gammaRad);
    let sG = Math.sin(gammaRad);
    // Calculating rA, rB, rC (Rotation Components)
    let rA = - cA * sG - sA * sB * cG;
    let rB = - sA * sG + cA * sB * cG;
    let rC = - cB * cG;
    // Calculating the Compass Heading
    let compassHeading = Math.atan(rA / rB);
    // Converting to whole Unit Circle
    if (rB < 0) {
      compassHeading += Math.PI;
    } else if (rA < 0) {
      compassHeading += 2 * Math.PI;
    }
    // Convert radians to degrees
    compassHeading *= 180 / Math.PI;
    // Rounding up the heading
    let roundedValue = Math.round(compassHeading);
    console.log("roundedValue: ", roundedValue);
    // Inversing the degrees by mapping it to use it for servo angles
    let angle = this.mappingHelper(roundedValue, 0, 360, 360, 0);
    console.log("angle: ", angle);
    return angle;
  }

  mappingHelper(num, in_min, in_max, out_min, out_max) {
    return Math.round((num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
  }
}
