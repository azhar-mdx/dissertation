import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'controller-selector', pathMatch: 'full' },
  { path: 'controller-selector', loadChildren: () => import('./pages/controller-selector/controller-selector.module').then(s => s.ControllerSelectorPageModule) },
  { path: 'head-movement-controller', loadChildren: './pages/head-movement-controller/head-movement-controller.module#HeadMovementControllerPageModule' },
  { path: 'navigation-controller', loadChildren: './pages/navigation-controller/navigation-controller.module#NavigationControllerPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
