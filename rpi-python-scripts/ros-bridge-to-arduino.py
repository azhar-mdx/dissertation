import roslibpy # importing ros library for python
import serial # importing serial to communicate with arduino

ser = serial.Serial('/dev/ttyUSB0', 115200) # connection to arduino


# callback function for pan data
def callbackPan(res):
	panData = res['data']
	print("Pan data: ", panData)
	concatData = str(panData) + "*"
	ser.write(concatData.encode()) # sending encoded data to arduino

# callback function for tilt data
def callbackTilt(res):
	tiltData = res['data']
	print("Tilt data: ", tiltData)
	concatData = str(tiltData) + "?"
	ser.write(concatData.encode()) # sending encoded data to arduino

# callback function for nav data
def callbackNav(res):
	navDirection = 0
	print("Nav data: ", res)

	if res['y'] < -2:
		print("Direction forward")
		navDirection = 1
	elif res['y'] > 2:
		print("Direction backward")
		navDirection = 2
	elif res['x'] < -2:
		print("Direction right")
		navDirection = 3
	elif res['x'] > 2:
		print("Direction left")
		navDirection = 4
	else:
		print("Direction stop")

	concatData = str(navDirection) + "$"
	ser.write(concatData.encode()) # sending encoded data to arduino


# connecting to ros bridge
client = roslibpy.Ros(host='192.168.10.39', port=9091)
client.run()

# creating topics
panSub = roslibpy.Topic(client, '/servo_pan', 'std_msgs/Int16')
panSub.subscribe(callbackPan)

tiltSub = roslibpy.Topic(client, '/servo_tilt', 'std_msgs/Int16')
tiltSub.subscribe(callbackTilt)

navSub = roslibpy.Topic(client, '/vr_robo_vel', 'geometry_msgs/Point')
navSub.subscribe(callbackNav)


try:
    while True:
        pass
except KeyboardInterrupt:
	client.terminate()