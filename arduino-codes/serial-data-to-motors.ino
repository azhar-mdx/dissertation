#include <Servo.h> // Including sero library

Servo myservoPan;  // create servo object to control pan servo
Servo myservoTilt; // create servo object to control tilt servo

// Defining pins and variables
#define pwmA 5
#define in1 6
#define in2 7

#define in3 10
#define in4 11
#define pwmB 12

#define servoPanPin 2
#define servoTiltPin 3

String serialData = "";

int pwmOutput = 255;

void setup()
{
  Serial.begin(115200);
  pinMode(pwmA, OUTPUT);
  pinMode(pwmB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  myservoPan.attach(servoPanPin);   // attaches the servo on pin 2 to the servo object
  myservoTilt.attach(servoTiltPin); // attaches the servo on pin 3 to the servo object
}

// running a loop
void loop()
{
  if (Serial.available() > 0) // checking if serial is avialable
  {
    char s = Serial.read(); // reading serial data
    if (s == '*')           // if the encoded data has found '*' which is used to identify pan data
    {
      int panVal = serialData.toInt(); // convert the data to int
      Serial.print("This is pan servo value: ");
      Serial.println(panVal);
      setPanServo(panVal); // set new angle of pan servo
      serialData = "";     // empty the serial data variable to store new data
    }
    else if (s == '?') // else if the encoded data has found '?' which is used to identify tilt data
    {
      int tiltVal = serialData.toInt(); // convert the data to int
      Serial.print("This is tilt servo value: ");
      Serial.println(tiltVal);
      setTiltServo(tiltVal); // set new angle of tilt servo
      serialData = "";       // empty the serial data variable to store new data
    }
    else if (s == '$') // else if the encoded data has found '$' which is used to identify navigation data
    {
      int velVal = serialData.toInt(); // convert the data to int
      Serial.print("This is vel value: ");
      Serial.println(velVal);
      setNavDirection(velVal); // set new nav direction
      serialData = "";         // empty the serial data variable to store new data
    }
    else
    {
      serialData += s;
    }
  }
}

// nav direction function
void setNavDirection(int navVal)
{
  if (navVal == 1)
  {
    forward();
  }
  else if (navVal == 2)
  {
    reverse();
  }
  else if (navVal == 3)
  {
    left();
  }
  else if (navVal == 4)
  {
    right();
  }
  else
  {
    stop_motors();
  }
}

void forward()
{
  analogWrite(pwmA, pwmOutput);
  analogWrite(pwmB, pwmOutput);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void reverse()
{
  analogWrite(pwmA, pwmOutput);
  analogWrite(pwmB, pwmOutput);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void left()
{
  analogWrite(pwmA, pwmOutput);
  analogWrite(pwmB, pwmOutput);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
}

void right()
{
  analogWrite(pwmA, pwmOutput);
  analogWrite(pwmB, pwmOutput);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
}

void stop_motors()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, HIGH);
}

void setPanServo(int pos)
{
  myservoPan.write(pos);
}

void setTiltServo(int pos)
{
  myservoTilt.write(pos);
}
